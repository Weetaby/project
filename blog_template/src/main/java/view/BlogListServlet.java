package view;

import common.Util;
import dao.Blog;
import dao.BlogDao;
import dao.User;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/blog_list.html")
public class BlogListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");

        //1.检测当前是否为登录状态，如果未登录则重定向到登陆页面
        User user = Util.checkLoginStatus(req);
        if(user==null){
            System.out.println("当前用户未登录");
            resp.sendRedirect("login.html");
            return;
        }

        //2.从数据库中拿到所有的博客列表
        BlogDao blogDao = new BlogDao();
        List<Blog> blogs = blogDao.selectAll();

        //3.通过模板引擎渲染页面
        ServletContext context = this.getServletContext();
        TemplateEngine engine = (TemplateEngine) context.getAttribute("engine");
        WebContext webContext = new WebContext(req,resp,context);
        webContext.setVariable("blogs",blogs);
        webContext.setVariable("user",user);
        webContext.setVariable("blogsSize",blogs.size());
        String html = engine.process("blog_list",webContext);
        resp.getWriter().write(html);
    }
}
