package common;

import dao.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Util {
    public static User checkLoginStatus(HttpServletRequest req){
        //判定当前是否处于已登录状态
        HttpSession session = req.getSession(false);
        if(session==null){
            return null;
        }
        User user = (User) session.getAttribute("user");
        if(user==null){
            return null;
        }
        return user;
    }
}
