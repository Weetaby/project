create database if not exists blog;

use blog;

-- 创建博客表
drop table if exists blog;
create table blog (
    --自增主键
    blogId int primary key auto_increment,
    title varchar(1024),
    --mediumtext存储比varchar更长的内容
    content mediumtext,
    postTime datetime,
    userId int
);

--创建用户表
drop table if exists user;
create table user (
    --自增主键
    userId int primary key auto_increment,
    --防止用户名重复
    username varchar(128) unique,
    password varchar(128)
);
