package compile;

//表示编译运行的结果，即用户点提交之后得到的信息
public class Answer {
    // 0 表示编译运行 ok. 1 表示编译出错. 2 表示运行异常
    private int error;
    // error 1 , reason 包含了编译错误的信息
    // error 2 , reason 包含了异常的调用栈信息
    private String reason;
    // 程序的标准输出
    private String stdout;
    // 程序的标准错误
    private String stderr;

    //生成Getting和Setting
    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStdout() {
        return stdout;
    }

    public void setStdout(String stdout) {
        this.stdout = stdout;
    }

    public String getStderr() {
        return stderr;
    }

    public void setStderr(String stderr) {
        this.stderr = stderr;
    }

    //生成toString()
    @Override
    public String toString() {
        return "Answer{" +
                "errno=" + error +
                ", reason='" + reason + '\'' +
                ", stdout='" + stdout + '\'' +
                ", stderr='" + stderr + '\'' +
                '}';
    }
}

