package compile;

// 表示要编译运行的源代码，即用户在网页上编辑的代码
public class Question {
    private String code;

    //生成Getting和Setting
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
