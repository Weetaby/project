package compile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

//创建进程, 执行javac,java等命令
public class CommandUtil {
    // run方法就用于进行创建进程并执行命令
    // cmd 表示要执行的命令, 如javac
    // stdoutFile 指定标准输出写到哪个文件中
    // stderrFile 执行标准错误写到哪个文件中
    public static int run(String cmd, String stdoutFile, String stderrFile) {
        try {
            //1.通过Runtime类得到Runtime实例，执行exec方法
            Process process = Runtime.getRuntime().exec(cmd);

            //2.获取到标准输出并写入到指定文件中
            if (stdoutFile != null) {
                InputStream stdoutFrom = process.getInputStream();
                FileOutputStream stdoutTo = new FileOutputStream(stdoutFile);
                while (true) {
                    int ch = stdoutFrom.read();
                    if (ch == -1) {
                        break;
                    }
                    stdoutTo.write(ch);
                }
                stdoutFrom.close();
                stdoutTo.close();
            }

            //3.获取到标准错误并写入到指定文件中
            if (stderrFile != null) {
                InputStream stderrFrom = process.getErrorStream();
                FileOutputStream stderrTo = new FileOutputStream(stderrFile);
                while (true) {
                    int ch = stderrFrom.read();
                    if (ch == -1) {
                        break;
                    }
                    stderrTo.write(ch);
                }
                stderrFrom.close();
                stderrTo.close();
            }

            //4.等待子进程结束，返回状态码
            int exitCode = process.waitFor();
            return exitCode;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 1;
    }
    /*
    public static void main(String[] args) {
        CommandUtil.run("javac", "stdout.txt", "stderr.txt");
    }
    */
}
