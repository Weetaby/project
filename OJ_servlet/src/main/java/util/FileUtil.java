package util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

//封装文件操作，让读写文件更方便
public class FileUtil {
    //将filePath路径下的文件内容读取出来并返回
    public static String readFile(String filePath) {
        StringBuilder result = new StringBuilder();
        try (FileInputStream fileInputStream = new FileInputStream(filePath);) {
            while (true) {
                int ch = fileInputStream.read();
                if (ch == -1) {
                    break;
                }
                result.append((char)ch);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    //将content中的内容写入到filePath路径下的文件里
    public static void writeFile(String filePath, String content) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath)) {
            //进行写文件操作
            fileOutputStream.write(content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
