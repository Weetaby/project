package util;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// 方便其他的代码随时能和数据库建立连接~
public class DBUtil {
    //表示数据库地址
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/oj?characterEncoding=utf8&useSSL=false";
    //表示数据库用户名
    private static final String USERNAME = "root";
    //表示数据库密码
    //private static final String PASSWORD = "weetaby7";
    private static final String PASSWORD = "";


    //创建一个单例的dataSource实例
    private static DataSource dataSource = null;

    //提供一个获取dataSource单例的方法
    private static DataSource getDataSource() {
        if (dataSource == null) {
            //双重校验锁
            synchronized (DBUtil.class){
                if (dataSource == null) {
                    dataSource = new MysqlDataSource();
                    ((MysqlDataSource)dataSource).setUrl(URL);
                    ((MysqlDataSource)dataSource).setUser(USERNAME);
                    ((MysqlDataSource)dataSource).setPassword(PASSWORD);
                }
            }
        }
        return dataSource;
    }

    //提供一个具体的方法获取到当前的数据库连接
    public static Connection getConnection() throws SQLException {
        return getDataSource().getConnection();
    }

    //释放资源
    public static void close(Connection connection, PreparedStatement statement, ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
