package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.Problem;
import dao.ProblemDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/problem")
public class ProblemServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();

    // 读取题目列表和读取指定题目详情
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(200);
        resp.setContentType("application/json; charset=utf-8");

        ProblemDAO problemDAO = new ProblemDAO();
        //尝试从req读取id参数
        String id = req.getParameter("id");
        if (id == null || "".equals(id)) {
            //查找题目列表
            List<Problem> problems = problemDAO.selectAll();
            String respString = objectMapper.writeValueAsString(problems);
            resp.getWriter().write(respString);
        } else {
            //查找指定题目详情
            Problem problem = problemDAO.selectOne(Integer.parseInt(id));
            String respString = objectMapper.writeValueAsString(problem);
            resp.getWriter().write(respString);
        }
    }

    /*@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 进行新增一个题目的数据
        // 1. 要读取到请求中的 body 数据
        // 进一步就可以插入数据库了
        String body = HttpUtil.readBody(req);
        // 2. 把读到的数据构造成 Problem 对象
        Problem problem = gson.fromJson(body, Problem.class);
        // 3. 把数据插入到数据库
        ProblemDAO problemDAO = new ProblemDAO();
        problemDAO.insert(problem);
        // 4. 返回一个结果给客户端
        resp.setStatus(200);
        resp.setContentType("application/json; charset=utf-8");
        resp.getWriter().write("{\"ok\": 1}");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 删除操作
        resp.setStatus(200);
        resp.setContentType("application/json; charset=utf-8");
        // 1. 先从 req 请求对象中读取出要删除的题目 id
        String id = req.getParameter("id");
        if (id == null || id.equals("")) {
            resp.getWriter().write("{\"ok\": 0, \"reason\": \"id 不存在\"}");
            return;
        }
        // 2. 调用数据库操作执行删除即可
        ProblemDAO problemDAO = new ProblemDAO();
        problemDAO.delete(Integer.parseInt(id));
        // 3. 返回一个删除结果
        resp.getWriter().write("{\"ok\": 1}");
    }*/
}
