import org.omg.SendingContext.RunTime;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class TextExec {
    public static void main(String[] args) throws IOException, InterruptedException {
        //Runtime在JVM中是一个单例
        Runtime runtime = Runtime.getRuntime();
        //Process表示进程,Runtime.exec方法是把指定路径的可执行程序，创建子进程并且执行
        Process process = runtime.exec("javac");

        //获取到子进程里的标准输出，写到一个文件里
        InputStream stdoutFrom = process.getInputStream();
        FileOutputStream stdoutTo = new FileOutputStream("stdout.txt");
        while(true){
            int ch = stdoutFrom.read();
            if(ch==-1){
                break;
            }
            stdoutTo.write(ch);
        }

        //关闭文件
        stdoutFrom.close();
        stdoutTo.close();

        //获取子进程的标准错误
        InputStream stderrFrom = process.getErrorStream();
        FileOutputStream stderrTo = new FileOutputStream("stderr.txt");
        while(true){
            int ch = stderrFrom.read();
            if(ch==-1){
                break;
            }
            stderrTo.write(ch);
        }

        //关闭文件
        stderrFrom.close();
        stderrTo.close();

        //通过process里的waitFor方法来实现进程的等待,类似于Thread.join
        //父进程执行到waitFor的时候就会阻塞，一直阻塞到子进程执行完毕为止
        //exitCode表示退出码，表示子进程的执行结果是否正常，正常退出为0，异常退出非0
        int exitCode = process.waitFor();
        System.out.println(exitCode);
    }
}
