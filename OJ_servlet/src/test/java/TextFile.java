import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TextFile {
    public static void main(String[] args) throws IOException {
        //源文件读取出来写入目标文件
        String srcPath = "C:\\files\\srcTest.txt";
        String destPath = "C:\\files\\destTest.txt";

        //打开用来读数据的源文件
        FileInputStream inputStream = new FileInputStream(srcPath);
        //打开用来写数据的目标文件
        FileOutputStream outputStream = new FileOutputStream(destPath);

        //循环的把源文件的内容按照字节读取出，写入目标文件
        while(true){
            int ch = inputStream.read();
            //读取完毕会返回EOF，用-1来表示
            if(ch==-1){
                break;
            }
            outputStream.write(ch);
        }

        //关闭文件
        inputStream.close();
        outputStream.close();
    }
}
