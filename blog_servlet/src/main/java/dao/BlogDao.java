package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//封装对于Blog表的操作
public class BlogDao {
    //1.新增一个博客
    public void insert(Blog blog){

        System.out.println("插入一个博客");

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "insert into blog values(null,?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, blog.getTitle());
            statement.setString(2, blog.getContent());
            statement.setTimestamp(3, blog.getPostTime());
            statement.setInt(4, blog.getUserId());
            //3.执行SQL
            int ret =statement.executeUpdate();
            if(ret==1){
                System.out.println("插入成功");
            }else{
                System.out.println("插入失败");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
          DBUtil.close(connection,statement,null);
        }
    }

    //2.获取所有博客列表
    public List<Blog> selectAll(){
        System.out.println("查找所有博客");

        List<Blog> blogs = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "select * from blog order by postTime desc";
            statement = connection.prepareStatement(sql);
            //3.执行SQL
            resultSet = statement.executeQuery();
            //4.遍历结果集合
            while(resultSet.next()){
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                String content  = resultSet.getString("content");
                //如果博客正文太长，就截取一部分内容
                if(content.length()>90){
                    content = content.substring(0,90) + "...";
                }
                blog.setContent(content);
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                blogs.add(blog);
            }
            return blogs;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //3.根据博客id获取指定博客
    public Blog selectOne(int blogId){
        System.out.println("查找指定博客");

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "select * from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //3.执行SQL
            resultSet = statement.executeQuery();
            //4.遍历结果集合
            if(resultSet.next()){
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blog.setUserId(resultSet.getInt("userId"));
                return blog;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //4.根据博客id删除指定博客
    public void delete(int blogId){
        System.out.println("删除指定博客");

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "delete from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //3.执行SQL
            int ret = statement.executeUpdate();
            if(ret==1){
                System.out.println("删除成功");
            }else {
                System.out.println("删除失败");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,null);
        }
    }
    /*
    public static void main(String[] args) {
        BlogDao blogDao = new BlogDao();
        //1.测试插入博客功能
        Blog blog = new Blog();
        blog.setTitle("博客标题");
        blog.setContent("博客正文");
        blog.setUserId(1);
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        blogDao.insert(blog);
        //2.测试查找所有博客功能
        List<Blog> blogs = blogDao.selectAll();
        System.out.println(blogs);
        //3.测试查找指定博客功能
        Blog blog = blogDao.selectOne(1);
        System.out.println(blog);
        //4.测试删除指定博客功能
        blogDao.delete(2);
    }
    */
}
