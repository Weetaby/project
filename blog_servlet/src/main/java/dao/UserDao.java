package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//封装对于User表的操作
public class UserDao {
    //1.新增用户（实现注册功能）
    public void insert(User user){
        System.out.println("插入一个用户");

        Connection connection = null;
        PreparedStatement statement = null;
        try {
            //1.建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "insert into user values(null,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            //3.执行SQL
            int ret =statement.executeUpdate();
            if(ret==1){
                System.out.println("插入成功");
            }else{
                System.out.println("插入失败");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,null);
        }
    }

    //2.根据用户名查找用户对象（实现登录功能）
    public User selectByName(String username){
        System.out.println("根据用户名查找指定用户");

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "select * from user where username = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1,username);
            //3.执行SQL
            resultSet = statement.executeQuery();
            //4.遍历结果集合
            if(resultSet.next()){
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    //3.根据用户ID查找用户对象（这个是后续在博客页面上显示作者的时候必要的方法）
    public User selectById(int userId){
        System.out.println("根据用户ID查找指定用户");

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            //1.建立连接
            connection = DBUtil.getConnection();
            //2.构造SQL语句
            String sql = "select * from user where userId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,userId);
            //3.执行SQL
            resultSet = statement.executeQuery();
            //4.遍历结果集合
            if(resultSet.next()){
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setUsername(resultSet.getString("username"));
                user.setPassword(resultSet.getString("password"));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }
    /*
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        //1.测试新增用户功能
        User user = new User();
        user.setUsername("admin");
        user.setPassword("123456");
        userDao.insert(user);
        //2.测试根据用户名查找用户功能
        User user = userDao.selectByName("1号用户");
        System.out.println(user);
        //3.测试根据用户ID查找用户功能
        User user = userDao.selectById(1);
        System.out.println(user);
    }
    */
}
