package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.Blog;
import dao.BlogDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/count")
public class CountServlet extends HttpServlet {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");

        //1.获取到blogs
        BlogDao blogDao = new BlogDao();
        List<Blog> blogs = blogDao.selectAll();
        //2.获取到当前的文章数
        int count = blogs.size();
        String jsonString = objectMapper.writeValueAsString(count);
        resp.getWriter().write(jsonString);
    }
}
