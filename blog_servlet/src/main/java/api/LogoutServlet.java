package api;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");

        //1.处理注销的逻辑
        HttpSession session = req.getSession(false);
        if (session == null) {
            // 当前用户都没有登录
            String html = "当前尚未登录!";
            resp.getWriter().write(html);
            return;
        }
        session.removeAttribute("user");

        //2.重定向到登录页面
        resp.sendRedirect("login.html");
    }
}
