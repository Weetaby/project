package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import common.Util;
import dao.Blog;
import dao.BlogDao;
import dao.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

//处理博客相关的请求
@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
    //Jackson库里的核心类
    private ObjectMapper objectMapper = new ObjectMapper();

    //处理博客列表和博客正文
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //数据就会自动转换成json数组
        resp.setContentType("application/json;charset=utf8");

        //1.监测用户登录状态
        User user = Util.checkLoginStatus(req);
        if (user == null) {
            resp.setStatus(403);
            String html = "<h3>当前用户未登录</h3>";
            resp.getWriter().write(html);
            return;
        }

        //2.根据是否能获取到blogId，对应的返回博客正文或博客列表
        String blogId = req.getParameter("blogId");
        BlogDao blogDao = new BlogDao();
        if (blogId == null) {
            //参数不存在,则返回博客列表
            List<Blog> blogs = blogDao.selectAll();
            String jsonString = objectMapper.writeValueAsString(blogs);
            resp.getWriter().write(jsonString);
        } else {
            //参数存在,则返回博客正文
            Blog blog = blogDao.selectOne(Integer.parseInt(blogId));
            String jsonString = objectMapper.writeValueAsString(blog);
            resp.getWriter().write(jsonString);
        }
    }

    //处理新增博客
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        //1、监测用户是否登录
        User user = Util.checkLoginStatus(req);
        if (user == null) {
            String html = "<h3>未登录不能发布博客</h3>";
            resp.getWriter().write(html);
            return;
        }

        //2、获取请求中的字段
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        if (title == null || "".equals(title) || content == null || "".equals(content)) {
            String html = "<h3>当前提交的博客标题或者正文缺失!</h3>";
            resp.getWriter().write(html);
            return;
        }

        // 3. 构造 Blog 对象, 插入到数据库中
        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setContent(content);
        blog.setUserId(user.getUserId());
        blog.setPostTime(new Timestamp(System.currentTimeMillis()));
        BlogDao blogDao = new BlogDao();
        blogDao.insert(blog);

        // 4. 插入成功之后, 让页面重定向到 blog_list.html
        resp.sendRedirect("blog_list.html");
    }

    //实现删除博客
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");
        //1.先检测登录状态
        User user = Util.checkLoginStatus(req);
        if (user == null) {
            String html = "<h3>当前尚未登录, 不能删除</h3>";
            resp.getWriter().write(html);
            return;
        }

        //2.读取出要删除的 blogId
        String blogId = req.getParameter("blogId");
        if (blogId == null || "".equals(blogId)) {
            String html = "<h3>blogId 参数缺失!</h3>";
            resp.getWriter().write(html);
            return;
        }

        //3.从数据库读取一下博客的作者, 验证一下当前登录的用户是否就是作者
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.selectOne(Integer.parseInt(blogId));
        if (blog == null) {
            String html = "<h3>要删除的博客不存在!</h3>";
            resp.getWriter().write(html);
            return;
        }
        if (blog.getUserId() != user.getUserId()) {
            String html = "<h3>无法删除其他人的博客!</h3>";
            resp.getWriter().write(html);
            return;
        }

        //4.执行删除操作
        blogDao.delete(Integer.parseInt(blogId));

        //5.删除完毕
        resp.setStatus(200);
        String html = "<h3>删除成功</h3>";
        resp.getWriter().write(html);
    }
}
