create database if not exists oj;

use oj;
--创建存储题目的表
--题号，题目，难度，题干，初始代码，测试用例

drop table if exists oj;
create table oj (
    id int primary key auto_increment,
    title varchar(50),
    level varchar(50),
    description varchar(4096),
    templateCode varchar(4096),
    testCode varchar(4096)
);