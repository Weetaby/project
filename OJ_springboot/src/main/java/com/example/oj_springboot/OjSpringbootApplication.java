package com.example.oj_springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OjSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(OjSpringbootApplication.class, args);
    }

}
