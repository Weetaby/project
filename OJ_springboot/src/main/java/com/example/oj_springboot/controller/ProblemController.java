package com.example.oj_springboot.controller;

import com.example.oj_springboot.config.NullException;
import com.example.oj_springboot.model.Problem;
import com.example.oj_springboot.service.ProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/problem")
public class ProblemController {

    @Autowired
    private ProblemService problemService;

    @GetMapping("/getOne")
    public Problem getProblem(String id) throws Exception {
        if(id==null||id.equals("")){
            throw new Exception();
        }
        return problemService.getOne(id);
    }

    @GetMapping("/getAll")
    public List<Problem> getProblems(){
        return problemService.getAll();
    }

    @PostMapping("/add")
    public void addProblem(Problem problem) throws NullException {
        if(problem.getTitle()==null||problem.getTitle().equals("")||
                problem.getDescription()==null||problem.getDescription().equals("")||
                problem.getTestCode()==null||problem.getTestCode().equals("")){
            throw new NullException();
        }
        problemService.add(problem);
    }

    @RequestMapping(value = "/delete",method = RequestMethod.DELETE)
    public void delete(String id) throws Exception {
        if(id==null||id.equals("")){
            throw new Exception();
        }
        problemService.delete(id);
    }
}
