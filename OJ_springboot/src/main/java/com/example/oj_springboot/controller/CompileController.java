package com.example.oj_springboot.controller;


import com.example.oj_springboot.config.NullException;
import com.example.oj_springboot.model.CompileRequest;
import com.example.oj_springboot.model.CompileResponse;
import com.example.oj_springboot.service.CompileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping
public class CompileController {

    @Autowired
    private CompileService compileService;

    @PostMapping("/compile")
    public CompileResponse compile(@RequestBody CompileRequest compileRequest) throws Exception {
        if(compileRequest.getCode()==null||compileRequest.getCode().equals("")){
            throw new NullException();
        }
        return compileService.compile(compileRequest);
    }
}
