package com.example.oj_springboot.mapper;

import com.example.oj_springboot.model.Problem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ProblemMapper {
    Problem getOne(String id);

    List<Problem> getAll();

    void add(Problem problem);

    void delete(String id);
}
