package com.example.oj_springboot.service;

import com.example.oj_springboot.mapper.ProblemMapper;
import com.example.oj_springboot.model.Problem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProblemService {

    @Resource
    private ProblemMapper problemMapper;

    public Problem getOne(String id) {
        return problemMapper.getOne(id);
    }

    public List<Problem> getAll() {
        return problemMapper.getAll();
    }

    public void add(Problem problem) {
        problemMapper.add(problem);
    }

    public void delete(String id) {
        problemMapper.delete(id);
    }
}
