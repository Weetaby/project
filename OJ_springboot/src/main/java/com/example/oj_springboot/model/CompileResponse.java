package com.example.oj_springboot.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CompileResponse {
    public int error;
    public String reason;
    public String stdout;
}
