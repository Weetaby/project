package com.example.oj_springboot.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Problem {
    // 题目的编号
    private int id;
    // 题目的标题
    private String title;
    // 题目的难度级别
    private String level;
    // 题目的详细描述
    private String description;
    // 题目的模板代码
    private String templateCode;
    // 题目的测试用例代码
    private String testCode;
}
