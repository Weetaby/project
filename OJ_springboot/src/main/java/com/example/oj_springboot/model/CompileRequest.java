package com.example.oj_springboot.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CompileRequest {
    public String id;
    public String code;
}
