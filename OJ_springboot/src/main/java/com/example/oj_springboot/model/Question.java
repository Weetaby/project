package com.example.oj_springboot.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
// 表示要编译运行的源代码，即用户在网页上编辑的代码
public class Question {
    private String code;
}
