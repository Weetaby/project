package com.example.oj_springboot.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.annotation.Scope;

@Getter
@Setter
@ToString
//表示编译运行的结果，即用户点提交之后得到的信息
public class Answer {
    // 0 表示编译运行 ok. 1 表示编译出错. 2 表示运行异常
    private int error;
    // error 1 , reason 包含了编译错误的信息
    // error 2 , reason 包含了异常的调用栈信息
    private String reason;
    // 程序的标准输出
    private String stdout;
    // 程序的标准错误
    private String stderr;
}

