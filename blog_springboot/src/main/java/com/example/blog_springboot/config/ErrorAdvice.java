package com.example.blog_springboot.config;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

/**
 * 统一异常处理
 */
@ControllerAdvice // 1.标识为控制器增强类
@ResponseBody
public class ErrorAdvice {

    // 2.添加异常统一处理方法
    @ExceptionHandler(NullException.class)
    public HashMap<String, Object> nullException(NullException e) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("code",-1);
        result.put("data", "提交的信息不能为空，请检查！");
        return result;
    }

    @ExceptionHandler(Exception.class)
    public HashMap<String, Object> exception(Exception e) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("code",-1);
        result.put("data", "服务发生异常，请尝试重新登录！");
        return result;
    }

    @ExceptionHandler(ErrorException.class)
    public HashMap<String, Object> errorException(ErrorException e) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("code",-1);
        result.put("data", "账号或密码错误！");
        return result;
    }

    @ExceptionHandler(RepeatException.class)
    public HashMap<String, Object> repeatException(RepeatException e) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("code",-1);
        result.put("data", "此用户名已存在！");
        return result;
    }
}
