package com.example.blog_springboot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 全局配置文件
 */
@Configuration
public class AppConfig implements WebMvcConfigurer {
    // 配置拦截器和拦截规则
    @Override
    public void addInterceptors(InterceptorRegistry registry) {registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**") // 拦截所有请求
                .excludePathPatterns("/**/*.html")
                .excludePathPatterns("/**/*.css")
                .excludePathPatterns("/**/*.js")
                .excludePathPatterns("/**/img/**")
                .excludePathPatterns("/user/login")
                .excludePathPatterns("/user/reg");
    }
    // 设置api统一的访问前缀
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.addPathPrefix("blog_system", c -> false);
    }
}
