package com.example.blog_springboot.mapper;

import com.example.blog_springboot.model.User;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserMapper {
    User selectByUsername(String username);

    User selectByUserId(int userId);

    void add(String username, String password);
}
