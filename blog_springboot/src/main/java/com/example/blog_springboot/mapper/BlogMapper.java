package com.example.blog_springboot.mapper;

import com.example.blog_springboot.model.Blog;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

@Mapper
public interface BlogMapper {
    List<Blog> selectAll();

    Blog selectOne(String blogId);

    void add(Blog blog);

    void delete(String blogId);
}
