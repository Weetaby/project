package com.example.blog_springboot.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class User {
    private int userId;
    private String username;
    private String password;
    private boolean yourBlog;
}
