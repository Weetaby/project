package com.example.blog_springboot.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

@Getter
@Setter
@ToString
public class Blog {
    private int blogId;
    private String title;
    private String content;
    //对应SQL里的postTime
    private Timestamp postTime;
    private int userId;
}
